#include <iostream>
#include <Eigen/Dense>
#include "Registration.h"



int main(int argc, char *argv[]) {
  Registration registration(argv[1], argv[2]);
  registration.draw_registration_result();
  std::cout<<registration.get_transformation()<<std::endl;

  registration.execute_global_registration();
  registration.draw_registration_result();
  std::cout<<registration.get_transformation()<<std::endl;

  registration.execute_icp_registration();
  registration.draw_registration_result();
  std::cout<<registration.get_transformation()<<std::endl;

  registration.write_tranformation_matrix(argv[3]);
  return 0;
}
