import yaml
import numpy as np
import functools
import operator
import copy


def duplicate_cameras(in_matrices, ex_matrices, transform):
    curr_num_cam = len(ex_matrices)
    ex_matrices_c = copy.deepcopy(ex_matrices)

    in_matrices_c = copy.deepcopy(in_matrices) + copy.deepcopy(in_matrices)
    for i in range(curr_num_cam):
        ex_matrices_c.append(np.matmul(transform, ex_matrices[i]))
    return in_matrices_c, ex_matrices_c, curr_num_cam


def add_virtual_cameras(in_matrices, ex_matrices, num_true_cam, transform):

    ex_matrices_c = copy.deepcopy(ex_matrices)

    in_matrices_c = copy.deepcopy(in_matrices) + copy.deepcopy(in_matrices[:num_true_cam])
    for i in range(num_true_cam):
        ex_matrices_c.append(np.matmul(transform, ex_matrices[i]))
    return in_matrices_c, ex_matrices_c


def transform_read(transform_path):
    matrix = np.zeros((4, 4), dtype=np.float32)
    with open(transform_path, 'r') as f:
        elems = f.read().split()

        for i in range(4):
            for j in range(4):
                matrix[i][j] = elems[j+i*4]

    f.close()
    return matrix


def scanner_read(yaml_file):
    in_matrices = []
    ex_matrices = []
    in_matrix = np.zeros((3, 3), dtype=float)
    ex_matrix = np.zeros((4, 4), dtype=float)

    with open(yaml_file, 'r') as file:
        camera_params = yaml.load(file, Loader=yaml.Loader)
        num_cam = len(camera_params["intrinsics"])
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format

    for i in range(num_cam):

        rot = camera_params["extrinsics"][i]["rotation"]["data"]
        translation = camera_params["extrinsics"][i]["translation"]["data"]
        in_matrix[0, 0] = camera_params["intrinsics"][i]["fx"]
        in_matrix[1, 1] = camera_params["intrinsics"][i]["fy"]
        in_matrix[0, 2] = camera_params["intrinsics"][i]["cx"]
        in_matrix[1, 2] = camera_params["intrinsics"][i]["cy"]
        in_matrix[2, :] = np.array([0., 0., 1.])
        for j in range(9):
            row_index = np.floor(j / 3).astype(int)
            col_index = np.floor(j % 3).astype(int)
            ex_matrix[row_index, col_index] = rot[j]

        for j in range(3):
            ex_matrix[j, 3] = translation[j]

        ex_matrix[3, :] = np.array([0., 0., 0., 1.])
      # append to output
        in_matrices.append(np.copy(in_matrix))
        ex_matrices.append(np.copy(ex_matrix))

    return in_matrices, ex_matrices


def scanner_read_raw_intrinsics(yaml_file):
    ex_matrices = []
    ex_matrix = np.zeros((4, 4), dtype=float)

    with open(yaml_file, 'r') as file:
        camera_params = yaml.load(file, Loader=yaml.Loader)
        num_cam = len(camera_params["intrinsics"])
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format

    for i in range(num_cam):

        rot = camera_params["extrinsics"][i]["rotation"]["data"]
        translation = camera_params["extrinsics"][i]["translation"]["data"]
        for j in range(9):
            row_index = np.floor(j / 3).astype(int)
            col_index = np.floor(j % 3).astype(int)
            ex_matrix[row_index, col_index] = rot[j]

        for j in range(3):
            ex_matrix[j, 3] = translation[j]
        ex_matrix[3, :] = np.array([0., 0., 0., 1.])
        ex_matrices.append(np.copy(ex_matrix))

    return camera_params["intrinsics"], ex_matrices


def scanner_write(raw_intrinsics, ex_matrices, yaml_file):
    with open(yaml_file, 'w+') as file:
        num_cam = len(ex_matrices)
        extrinsics = []
        for i in range(num_cam):

            ex_rot = ex_matrices[i][:3, :3].flatten().tolist()
            ex_trans = ex_matrices[i][:3, 3].flatten().tolist()
            extrinsics.append({'rotation': {'rows': 3, 'data': ex_rot, 'dt': 'd', 'cols': 3},
                               'translation': {'data': ex_trans, 'cols': 1, 'dt': 'd', 'rows': 3}})

        raw_intrinsics=raw_intrinsics.copy()
        camera_params = {'trigger_dev': '/dev/trigger', 'cameras': {'num_devices': num_cam, 'devices': 'VIRTUAL'},
                         'extrinsics': extrinsics, 'intrinsics': raw_intrinsics, 'profile': 'VIRTUAL'}
        yaml.dump(camera_params, file, default_flow_style=False)

    return in_matrices, ex_matrices


if __name__ == "__main__":
    transform = transform_read("/home/flexsight/Downloads/acquisition_giacca/transform.txt")
    in_matrices, ex_matrices = scanner_read("/home/flexsight/FlexSight/unity_sim/Assets/Resources/config/scanner_config.yml")
    raw_intrinsics, ex_matrices = scanner_read_raw_intrinsics("/home/flexsight/FlexSight/unity_sim/Assets/Resources/config/scanner_config.yml")
    vraw_intrinsics, vex_matrices, num_cams = duplicate_cameras(raw_intrinsics, ex_matrices, transform)
    scanner_write(vraw_intrinsics, vex_matrices, "/home/flexsight/Downloads/acquisition_giacca/scanner_config.yml")

    raw_intrinsics, ex_matrices = scanner_read_raw_intrinsics("/home/flexsight/Downloads/acquisition_giacca/scanner_config.yml")
    vraw_intrinsics, vex_matrices = add_virtual_cameras(raw_intrinsics, ex_matrices, num_cams, transform)
    scanner_write(vraw_intrinsics, vex_matrices, "/home/flexsight/Downloads/acquisition_giacca/scanner_config2.yml")

