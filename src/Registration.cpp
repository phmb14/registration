//
// Created by flexsight on 16/12/21.
//

#include "Registration.h"


Registration::Registration(std::string cloud_source_filename, std::string cloud_target_filename)
{
  open3d::io::ReadPointCloud(cloud_source_filename, source_ );
  open3d::io::ReadPointCloud(cloud_target_filename, target_ );
}


Registration::Registration(open3d::geometry::PointCloud cloud_source, open3d::geometry::PointCloud cloud_target)
{
  source_ = cloud_source;
  target_ = cloud_target;
}


void Registration::draw_registration_result()
{
  //clone input
  open3d::geometry::PointCloud source_clone = source_;
  open3d::geometry::PointCloud target_clone = target_;

  //different color
  Eigen::Vector3d color_s;
  Eigen::Vector3d color_t;
  color_s<<1, 0.706, 0;
  color_t<<0, 0.651, 0.929;

  target_clone.PaintUniformColor(color_s);
  source_clone.PaintUniformColor(color_t);
  source_clone.Transform(transformation_);

  auto src_pointer =  std::make_shared<open3d::geometry::PointCloud>(source_clone);
  auto target_pointer =  std::make_shared<open3d::geometry::PointCloud>(target_clone);
  open3d::visualization::DrawGeometries({src_pointer, target_pointer});
  return;
}


void Registration::preprocess(open3d::geometry::PointCloud pcd, double voxel_size, std::shared_ptr<open3d::geometry::PointCloud> &pcd_down_ptr, std::shared_ptr<open3d::pipelines::registration::Feature> &pcd_fpfh)
{
  pcd_down_ptr = pcd.VoxelDownSample(voxel_size);
  double radius_normal = voxel_size * 2;
  pcd_down_ptr->EstimateNormals(open3d::geometry::KDTreeSearchParamHybrid(radius_normal, 30));
  double radius_feature = voxel_size * 5;
  pcd_fpfh = open3d::pipelines::registration::ComputeFPFHFeature(*pcd_down_ptr,
                                                                 open3d::geometry::KDTreeSearchParamHybrid(radius_feature, 100));
  return;
}

open3d::pipelines::registration::RegistrationResult Registration::execute_global_registration(double voxel_size)
{
  open3d::geometry::PointCloud source_transformed = source_;
  source_transformed.Transform(transformation_);

  std::shared_ptr<open3d::geometry::PointCloud> source_down_ptr;
  std::shared_ptr<open3d::geometry::PointCloud> target_down_ptr;

  std::shared_ptr<open3d::pipelines::registration::Feature> source_fpfh_ptr;
  std::shared_ptr<open3d::pipelines::registration::Feature> target_fpfh_ptr;

  Registration::preprocess(source_transformed, voxel_size, source_down_ptr, source_fpfh_ptr);
  Registration::preprocess(target_, voxel_size, target_down_ptr, target_fpfh_ptr);

  float distance_threshold = voxel_size * 1.5;

  auto cc_edge_length =  open3d::pipelines::registration::CorrespondenceCheckerBasedOnEdgeLength(0.9);
  auto cc_distance = open3d::pipelines::registration::CorrespondenceCheckerBasedOnDistance(distance_threshold);
  auto result = open3d::pipelines::registration::RegistrationRANSACBasedOnFeatureMatching(*source_down_ptr, *target_down_ptr, *source_fpfh_ptr, *target_fpfh_ptr,
                                                                                          true, distance_threshold,
                                                                                          open3d::pipelines::registration::TransformationEstimationPointToPoint(), 3,
                                                                                          {cc_edge_length, cc_distance}, open3d::pipelines::registration::RANSACConvergenceCriteria(100000,0.999));
  transformation_ = transformation_*result.transformation_;

  return result;
}

open3d::pipelines::registration::RegistrationResult Registration::execute_icp_registration(double threshold, double relative_fitness, double relative_rmse, int max_iteration)
{

  auto result = open3d::pipelines::registration::RegistrationICP(source_, target_,
                                                                     threshold,
                                                                     transformation_,
                                                                     open3d::pipelines::registration::TransformationEstimationPointToPoint(),
                                                                     open3d::pipelines::registration::ICPConvergenceCriteria(relative_fitness, relative_rmse, max_iteration));
  transformation_ = result.transformation_;
  return result;
}


void Registration::set_transformation(Eigen::Matrix4d init_transformation)
{
  transformation_=init_transformation;
}


Eigen::Matrix4d  Registration::get_transformation()
{
  return transformation_;
}


void Registration::write_tranformation_matrix(std::string filename)
{
  std::ofstream outfile (filename);
  if (outfile.is_open())
  {
    outfile << transformation_;
    outfile.close();
  }
}


